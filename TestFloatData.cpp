//
// Created by Fatemeh on 2/9/2020.
//
//
//

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <sys/time.h>
#include "RTree.h"
using namespace std;


typedef float ValueType;

struct Rect
{
    Rect()  {}

    Rect(float a_minX, float a_minY, float a_maxX, float a_maxY)
    {
        m_min[0] = a_minX;
        m_min[1] = a_minY;
        m_max[0] = a_maxX;
        m_max[1] = a_maxY;
    }


    float m_min[2];
    float m_max[2];
};


double timing(){
    static struct timeval t1, t2;
    gettimeofday(&t2,NULL);
    double ret = t2.tv_sec - t1.tv_sec + (t2.tv_usec - t1.tv_usec) * 1e-6;
    t1 = t2;
    return ret;
}

int main(int argc, char **argv){

    srand(time(0));
// So the order of args are: data_file, query_size, query_file, all_files_flag, times_file_name

    string data_file_name = argv[1];
    int query_size = stoi(argv[2]);
    string query_file_name = argv[3];
    string time_file_name = argv[4];
    

    std::ifstream query_file(query_file_name.c_str());

    int query_count = std::count(std::istreambuf_iterator<char>(query_file),
                                 std::istreambuf_iterator<char>(), '\n');
    if(query_count < query_size) {cout<<"NOT ENOUGH QUERIES IN THE FILE\n"; return 1;}

    ofstream times_file;
    times_file.open(time_file_name.c_str());

    typedef RTree<ValueType, 16, 8, 64> MyTree;
    MyTree tree(data_file_name);

    float min_x, min_y, max_x, max_y;
    float min[2]; float max[2];
    
    cout << "INSERTION COMPLETE\n";

    query_file.clear();
    query_file.seekg(0, ios::beg);
    Rect queries[query_size];

    for(int i = 0; i < query_size; i++){
        query_file >> min_x >> min_y >> max_x >> max_y;
        queries[i] = Rect(min_x, min_y, max_x, max_y);
    }
    int found_count; 
    double q_time = 0;
    timing();
    for(int i = 0; i < query_size; i++){
        timing();
        found_count = tree.QueryAdaptive(queries[i].m_min, queries[i].m_max);
        q_time = timing();
        times_file << q_time << "\n";
        cout << "QUERY " << i << " " << found_count << "\n";
        
    }

    query_file.close();
    times_file.close();
    cout << "DONE!!\n";
    return 0;
}

