cd data
cat data.?? > ../synthetic_uniformly_located_exponentialy_sized_data.txt
cd ..
g++ -O3 -o folan TestFloatData.cpp -std=c++14
#./folan data_file.txt 1000 query_file.txt time_file.txt > log_file.txt
./folan synthetic_uniformly_located_exponentialy_sized_data.txt 100000 synthetic_uniformly_located_uniformly_sized_workload.txt time_file.txt > log_file.txt
