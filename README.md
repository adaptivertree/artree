# AR-tree


This project is an implementation of the Adaptive R-Tree idea. It is based off the RT-Tree implementation by [Superliminal](https://superliminal.com/sources/). I have edited the structures and added new content to accommodate the *adaptiveness* of the index.



## Requirements

About 5 GB of space and a `C++` compiler:)

## Running an Experiment

To run an experiment, you first need to set some parameters in the `RTree.h` file. Two constant values `DATA_COUNT` and `NUMDIMS` need to be set to the size of the data set and its dimensionality. Then just input the necessary information in the `run.sh` file and run it. The name of the data file, size of the workload, name of the file where the response times are to be written and name of the file to hold the log of the process have to be filled out.


## What the run.sh file does

As the data file was too large, it was split into pieces, so we first have to concatenate those back together. This is done in the first three lines.

Then `run` just compiles `TestFloatData.cpp`, with a >-O3 flag. 
Then we just run the experiment with the proper input. There are several input parameters that need to be set. Some are part of the runtime arguments some have to be hardcoded.
1. AR-Tree parameters: **maximum fanout** and **maximum leaf threhsold** --> must be hardcoded in the `TestFloatData.cpp` file. In the current state of the code they are set as 16 and 64 respectively.
2. **Size of data-set** --> has to be hadcoded in the `RTree.h` file. It is the `DATA_COUNT` variable defined in the start of the file. Currently it is set as 64M to match the example data-set.
3. **Dimensionality of data-set** --> has to be hadcoded in the `RTree.h` file. It is the `NUMDIMS` variable defined in the start of the file. Currently it is set as 2 to match the example data-set.
4. **Location of data-set file** --> This is a runtime argument that should be provided. In the example in `run.sh` it is currently set as `synthetic_uniformly_located_exponentialy_sized_data.txt`.
5. **Size of Workload** -->  This is a runtime argument that should be provided. In the example in `run.sh` it is set as 100000.
6. **Location of Workload file** --> This is also a runtime argument. In `run.sh` it is set as `synthetic_uniformly_located_uniformly_sized_workload.txt`.
7. **Location of query response time file** --> This is also a runtime argument. In the current `run.sh` it is set as `time_file.txt`. The response time of each query will be logged in this file.
8. **Location of log file** --> just where you would want the log of the workload to be stored. The log will inlude a few lines of preliminary information and then the count of result objects for each query.


*Warning* If you want to use the code for datasets of higher dimensioanlity, the `TestFloatData.cpp` will have to be adjusted to read and store the queries correctly. Currently the `Rect` struct is set to have 2 dimensions, and the loop that reads queries from file in the `main` function reads only two values. There is no need for any other change in the `RTree.h` file for the data except setting the `NUMDIMS` variable correctly.


## Data Format

The data and workload should be in this format for an example 2d data set.
> min_x min_y max_x max_y


For an aribitrary dimensionality, each line(each data object) should first state the minimum of the range of each dimension and then their maximum respectively.
