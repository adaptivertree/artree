#ifndef RTREE
#define RTREE


// NOTE This file compiles under MSVC 6 SP5 and MSVC .Net 2003 it may not work on other compilers without modification.

// NOTE These next few lines may be win32 specific, you may need to modify them to compile on other platform
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <stdlib.h>
#include <cstdlib>

#include <algorithm>
#include <functional>
#include <vector>

// TODO delete later
#include <iostream>

using namespace std;

#include <utility>
#include <queue>
#include <string>
#include <chrono>
#include <string.h>

#define ASSERT assert // RTree uses ASSERT( condition )
#ifndef Min
#define Min std::min
#endif //Min
#ifndef Max
#define Max std::max
#endif //Max

//
// RTree.h
//

#define RTREE_TEMPLATE template<class DATATYPE, int TMAXNODES, int TMINNODES, int TMAXDATAPOINTS, int TMINDATAPOINTS>
#define RTREE_QUAL RTree<DATATYPE, TMAXNODES, TMINNODES, TMAXDATAPOINTS, TMINDATAPOINTS>

#define RTREE_DONT_USE_MEMPOOLS // This version does not contain a fixed memory allocator, fill in lines with EXAMPLE to implement one.
//#define RTREE_USE_SPHERICAL_VOLUME // Better split classification, may be slower on some systems

#define DATA_COUNT 64000000
#define NUMDIMS 2

static float m_data_arr_mins[DATA_COUNT][NUMDIMS];
static float m_data_arr_maxes[DATA_COUNT][NUMDIMS];
static int m_data_arr_ids[DATA_COUNT];


template<class DATATYPE,
        int TMAXNODES = 8, int TMINNODES = TMAXNODES / 2, int TMAXDATAPOINTS = 256, int TMINDATAPOINTS = TMAXDATAPOINTS / 2>
                class RTree {
                protected:

                    struct Node;  // Fwd decl.  Used by other internal structs and iterator

                public:

                    // These constant must be declared after Branch and before Node struct
                    // Stuck up here for MSVC 6 compiler.  NSVC .NET 2003 is much happier.
                    enum {
                        MAXNODES = TMAXNODES,                         ///< Max elements in node
                        MINNODES = TMINNODES,                         ///< Min elements in node
                        MAXDATAPOINTS = TMAXDATAPOINTS,
                        MINDATAPOINTS = TMINDATAPOINTS
                    };



                public:

                    // initialize rtree from data_file
                    // also do it with static memory
                    RTree(std::string data_file_name);

                    virtual ~RTree();

                    /// Insert entry
                    /// \param a_min Min of bounding rect
                    /// \param a_max Max of bounding rect
                    /// \param a_dataId Positive Id of data.  Maybe zero, but negative numbers not allowed.
                    void Insert(const float a_min[NUMDIMS], const float a_max[NUMDIMS], const DATATYPE &a_dataId);

                    int CountNodes();
                    int TreeHeight();

                    int QueryAdaptive(const float a_min[NUMDIMS], const float a_max[NUMDIMS]);
                    /// Remove all entries from tree
                    void RemoveAll();
                    

                protected:

                    /// Minimal bounding rectangle (n-dimensional)
                    struct Rect {
                        Rect(){}
                        Rect(float mins[NUMDIMS], float maxes[NUMDIMS]){
                            for(int i=0; i< NUMDIMS; i++) {
                                m_min[i] = mins[i];
                                m_max[i] = maxes[i];
                            }
                        }
                        float m_min[NUMDIMS];                      ///< Min dimensions of bounding box
                        float m_max[NUMDIMS];                      ///< Max dimensions of bounding box
                    };

                    /// May be data or may be another subtree
                    /// The parents level determines this.
                    /// If the parents level is 0, then this is data
                    struct Branch {
                        Rect m_rect;                                  ///< Bounds
                        Node *m_child;                                ///< Child node
                        DATATYPE m_data;                              ///< Data Id
                        Branch(){}
                        Branch(Node *the_node, Rect the_rect, DATATYPE the_data){
                            m_child = the_node;
                            m_data = the_data;
                            for(int i=0; i< NUMDIMS; i++) {
                                m_rect.m_min[i] = the_rect.m_min[i];
                                m_rect.m_max[i] = the_rect.m_max[i];
                            }
                        }
                    };

                    /// Node for each branch level
                    struct Node {
                        bool IsInternalNode() { return (m_level > 0); } // Not a leaf, but a internal node
                        bool IsLeaf() { return (m_level == 0); } // A leaf, contains data
                        bool isRegular() { return m_regular; } // leaf is regular is m_regular is true, irregular ow

                        int m_count;                                  ///< Count
                        int m_level;                                  ///< Leaf is zero, others positive
                        bool m_regular;                               ///< Some leaves can be irregular. Leaf is irregular if this is false, regular ow
                        //        std::vector<Branch> m_branch;                 ///< Branch
                        Branch m_branch[MAXNODES];                    ///< Branch
//                        Branch m_data_points[MAXDATAPOINTS];            ///< data points, couldnt keep them in m_branch since the limits differ
                        int m_L;                                        ///< left side of data_points interval in m_data_arr
                        int m_R;                                        ///< right side of data_points interval in m_data_arr
                        Node* m_parent;
                        int m_id;

                        Node(){}
                    };

                    /// Variables for finding a split partition
                    struct PartitionVars {
                        enum {
                            NOT_TAKEN = -1
                        }; // indicates that position

                        int m_partition[MAXDATAPOINTS + 1];
                        //        std::vector<int> m_partition;
                        int m_total;
                        int m_minFill;
                        int m_count[2];
                        Rect m_cover[2];
                        float m_area[2];

                        Branch m_branchBuf[MAXDATAPOINTS + 1];
                        //        std::vector<Branch> m_branchBuf;
                        int m_branchCount;
                        Rect m_coverSplit;
                        float m_coverSplitArea;
                    };

                    struct LTA{
                        Node* this_leaf;
                        int Ls[2*NUMDIMS + 1];
                        int Rs[2*NUMDIMS + 1];
                        float crack_covers_mins[2*NUMDIMS + 1][NUMDIMS];
                        float crack_covers_maxes[2*NUMDIMS + 1][NUMDIMS];
                        int how_many_created = 0;

                    };

                    typedef vector<LTA> LeavesToAdd;

                    Node *AllocNode();

                    void FreeNode(Node *a_node);

                    void InitNode(Node *a_node);

                    void InitRect(Rect *a_rect);

                    bool InsertRectRec(const Branch &a_branch, Node *a_node, Node **a_newNode, int a_level);

                    bool InsertRectRec(const Branch &a_branch, Node *a_node, Node **a_newNode, int a_level, Rect &a_coverRect, Rect &a_newCoverRect);

                    bool InsertRect(const Branch &a_branch, Node **a_root, int a_level);

                    void Insert_anylevel(const Branch &a_branch, Node *start, int a_level);

                    int getNodeBranchIndex(Node* a_node);

                    Rect NodeCover(Node *a_node);

                    Rect LeafCover(int L, int R);

                    bool AddBranch(const Branch *a_branch, Node *a_node, Node **a_newNode);

                    bool AddBranch(const Branch *a_branch, Node *a_node, Node **a_newNode, Rect &a_coverRect, Rect &a_newCoverRect);

                    void DisconnectBranch(Node *a_node, int a_index);

                    int PickBranch(const Rect *a_rect, Node *a_node);

                    static Rect CombineRect(const Rect *a_rectA, const Rect *a_rectB);

                    void SplitNode(Node *a_node, const Branch *a_branch, Node **a_newNode);

                    void SplitNode(Node *a_node, const Branch *a_branch, Node **a_newNode, Rect &a_coverRect, Rect &a_newCoverRect);

                    float RectSphericalVolume(Rect *a_rect);

                    float RectVolume(Rect *a_rect);

                    float CalcRectVolume(Rect *a_rect);

                    void GetBranches(Node *a_node, const Branch *a_branch, PartitionVars *a_parVars);

                    void ChoosePartition(PartitionVars *a_parVars, int a_minFill);

                    void LoadNodes(Node *a_nodeA, Node *a_nodeB, PartitionVars *a_parVars);

                    void InitParVars(PartitionVars *a_parVars, int a_maxRects, int a_minFill);

                    void PickSeeds(PartitionVars *a_parVars);

                    void Classify(int a_index, int a_group, PartitionVars *a_parVars);

                    bool Overlap(Rect *a_rectA, Rect *a_rectB) const;

                    bool Overlap(Rect *a_rectA, const float a_min[NUMDIMS], const float a_max[NUMDIMS]) const;

                    bool Search(Node *a_node, Rect *a_rect, int &a_foundCount, LeavesToAdd *all_lta);

                    void Add_ltas(LeavesToAdd *all_lta);

                    bool CountNodes(Node *a_node, int &a_foundCount);

                    void RemoveAllRec(Node *a_node);

                    void Reset();

                    void CopyRec(Node *current, Node *other);

                    void CopyBranch(Branch &current, const Branch &other);
                    // returns 2*axis + {0 if min or 1 if max}, for all choices ignore = -3
                    int ChooseAxisLargestSideMid(Rect node_cover, Rect query_cover);
                    // returns the index where the crack is at
                    int CrackOnAxisCompMin( int L, int R, const float& crack_value, int axis, Rect *left_rect, Rect *right_rect);
                    int CrackOnAxisCompMax( int L, int R, const float& crack_value, int axis, Rect *left_rect, Rect *right_rect);
                    void swap_index(int i, int j);
                    // for in line node cover
                    int MyPartition(int L, int R, float pivot_value, int axis, bool min_or_max, Rect *left_rect, Rect *right_rect);

                    
                    Node *m_root;                                    ///< Root of tree
                    float m_unitSphereVolume;                 ///< Unit sphere constant for required number of dimensions
                    int last_id;
                    Rect root_cover;

                };


RTREE_TEMPLATE
RTREE_QUAL::RTree(std::string data_file_name){
    std::ifstream data_file(data_file_name.c_str());
    if(data_file.is_open()){cout << data_file_name << endl;}
    int data_count = std::count(std::istreambuf_iterator<char>(data_file),
                                std::istreambuf_iterator<char>(), '\n');
    if(data_count < DATA_COUNT){
        cout << "NOT ENOUGH DATA!!\n";
        exit(3);
    }

    cout << "size of node " << sizeof(Node) << endl;
    cout << "size of branch " << sizeof(Branch) << endl;
    data_file.clear();
    data_file.seekg(0, ios::beg);

    float min[NUMDIMS]; float max[NUMDIMS];

    for(int i = 0; i < DATA_COUNT; i++){


        for(int j = 0; j < NUMDIMS; j++){
            data_file >> min[j];
        }
        for(int j = 0; j < NUMDIMS; j++){
            data_file >> max[j];
        }


        m_data_arr_ids[i] = i;
        for (int axis = 0; axis < NUMDIMS; ++axis) {
            m_data_arr_mins[i][axis] = min[axis];
            m_data_arr_maxes[i][axis] = max[axis];
        }

    }

    data_file.close();

    m_root = AllocNode();
    m_root->m_level = 0;
    m_root->m_regular = false;
    m_root->m_parent = NULL;
    last_id = 0;
    m_root->m_id = last_id;

    last_id++;

    m_root->m_L = 0;
    m_root->m_R = DATA_COUNT;

    root_cover = NodeCover(m_root);

}


RTREE_TEMPLATE
RTREE_QUAL::~RTree() {
    Reset(); // Free, or reset node memory
}


RTREE_TEMPLATE
void RTREE_QUAL::Insert(const float a_min[NUMDIMS], const float a_max[NUMDIMS], const DATATYPE &a_dataId) {


    Branch branch;
    branch.m_data = a_dataId;
    branch.m_child = NULL;

    for (int axis = 0; axis < NUMDIMS; ++axis) {
        branch.m_rect.m_min[axis] = a_min[axis];
        branch.m_rect.m_max[axis] = a_max[axis];
    }

    InsertRect(branch, &m_root, 0);
}


RTREE_TEMPLATE
int RTREE_QUAL::CountNodes() {
    int foundCount = 0;
    CountNodes(m_root, foundCount);
    return foundCount;
}


RTREE_TEMPLATE
int RTREE_QUAL::TreeHeight() {
    return (m_root->m_level + 1);
}


RTREE_TEMPLATE
int RTREE_QUAL::QueryAdaptive(const float *a_min, const float *a_max) {

    Rect rect;

    for (int axis = 0; axis < NUMDIMS; ++axis) {
        rect.m_min[axis] = a_min[axis];
        rect.m_max[axis] = a_max[axis];
    }


    int foundCount = 0;
    LeavesToAdd all_lta;

    Search(m_root, &rect, foundCount, &all_lta);
    Add_ltas(&all_lta);

    return foundCount;

}


RTREE_TEMPLATE
void RTREE_QUAL::CopyRec(Node *current, Node *other) {
    //    cout << "in copyrec \n";
    current->m_level = other->m_level;
    current->m_count = other->m_count;

    if (current->IsInternalNode())  // not a leaf node
        {
        //        cout << "in copyrec not leaf m_count =  " << current->m_count<<"\n";
        for (int index = 0; index < current->m_count; ++index) {
            //            Branch *currentBranch;
            //            current->m_branch.push_back(*currentBranch);
            Branch *currentBranch = &current->m_branch[index];
            Branch *otherBranch = &other->m_branch[index];
            //            Branch *otherBranch;
            // TODO check the pointers later
            //            other->m_branch.push_back(*otherBranch);
            //            Branch *otherBranch = &other->m_branch[index];

            std::copy(otherBranch->m_rect.m_min,
                      otherBranch->m_rect.m_min + NUMDIMS,
                      currentBranch->m_rect.m_min);

            std::copy(otherBranch->m_rect.m_max,
                      otherBranch->m_rect.m_max + NUMDIMS,
                      currentBranch->m_rect.m_max);

            currentBranch->m_child = AllocNode();
            CopyRec(currentBranch->m_child, otherBranch->m_child);
        }
        } else // A leaf node
        {
        for (int index = 0; index < current->m_count; ++index) {


            //            Branch *currentBranch;
            //            current->m_branch.push_back(*currentBranch);
            Branch *currentBranch = &current->m_branch[index];
            Branch *otherBranch = &other->m_branch[index];
            //            Branch *otherBranch;
            // TODO check the pointers later
            //            other->m_branch.push_back(*otherBranch);
            //            Branch *otherBranch = &other->m_branch[index];


            //            for (int j = 0; j < NUMDIMS; j++) {
            //                currentBranch->m_rect.m_min[j] = otherBranch->m_rect.m_min[j];
            //            }
            std::copy(otherBranch->m_rect.m_min,
                      otherBranch->m_rect.m_min + NUMDIMS,
                      currentBranch->m_rect.m_min);


            std::copy(otherBranch->m_rect.m_max,
                      otherBranch->m_rect.m_max + NUMDIMS,
                      currentBranch->m_rect.m_max);


            currentBranch->m_data = otherBranch->m_data;
            //            current->m_branch.push_back(*currentBranch);
        }
        }
}


RTREE_TEMPLATE
void RTREE_QUAL::RemoveAll() {
    // Delete all existing nodes
    Reset();

    m_root = AllocNode();
    m_root->m_level = 0;
}


RTREE_TEMPLATE
void RTREE_QUAL::Reset() {
#ifdef RTREE_DONT_USE_MEMPOOLS
    // Delete all existing nodes
    RemoveAllRec(m_root);
#else // RTREE_DONT_USE_MEMPOOLS
    // Just reset memory pools.  We are not using complex types
    // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::RemoveAllRec(Node *a_node) {

    //    printf("HEEEEEY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
    //            ASSERT(a_node);
    //            ASSERT(a_node->m_level >= 0);
    // I think we have to make this in decreasing order and it shoudl be fine, but Im testing for now
    if (a_node->IsInternalNode()) // This is an internal node in the tree
        {
        //        printf("calling NC in RempveAllRec\n");
        Rect for_me = NodeCover(a_node);
        for (int index = 0; index < a_node->m_count; ++index) {
            //            printf("removing child %d with level: %d from node: %d, %d, %d, %d\n", index, a_node->m_branch[index].m_child->m_level, for_me.m_min[0], for_me.m_min[1], for_me.m_max[0], for_me.m_max[1]);
            RemoveAllRec(a_node->m_branch[index].m_child);
        }
        }
    FreeNode(a_node);
}


RTREE_TEMPLATE
typename RTREE_QUAL::Node *RTREE_QUAL::AllocNode() {
    Node *newNode;
#ifdef RTREE_DONT_USE_MEMPOOLS
    newNode = new Node;
#else // RTREE_DONT_USE_MEMPOOLS
    // EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
InitNode(newNode);
return newNode;
}


RTREE_TEMPLATE
void RTREE_QUAL::FreeNode(Node *a_node) {
    //            ASSERT(a_node);

#ifdef RTREE_DONT_USE_MEMPOOLS
delete a_node;
#else // RTREE_DONT_USE_MEMPOOLS
// EXAMPLE
#endif // RTREE_DONT_USE_MEMPOOLS
}


RTREE_TEMPLATE
void RTREE_QUAL::InitNode(Node *a_node) {
    a_node->m_count = 0;
    a_node->m_level = -1;
    a_node->m_regular = true;
    a_node->m_id = last_id;
    last_id++;
}


RTREE_TEMPLATE
void RTREE_QUAL::InitRect(Rect *a_rect) {
    for (int index = 0; index < NUMDIMS; ++index) {
        a_rect->m_min[index] = (float) 0;
        a_rect->m_max[index] = (float) 0;
    }
}


// Inserts a new data rectangle into the index structure.
// Recursively descends tree, propagates splits back up.
// Returns 0 if node was not split.  Old node updated.
// If node was split, returns 1 and sets the pointer pointed to by
// new_node to point to the new node.  Old node updated to become one of two.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRectRec(const Branch &a_branch, Node *a_node, Node **a_newNode, int a_level) {
    //            ASSERT(a_node && a_newNode);
    //            ASSERT(a_level >= 0 && a_level <= a_node->m_level);

    // recurse until we reach the correct level for the new record. data records
    // will always be called with a_level == 0 (leaf)
    if (a_node->m_level > a_level) {
        // Still above level for insertion, go down tree recursively
        Node *otherNode;

        // find the optimal branch for this record
        int index = PickBranch(&a_branch.m_rect, a_node);

        // recursively insert this record into the picked branch
        bool childWasSplit = InsertRectRec(a_branch, a_node->m_branch[index].m_child, &otherNode, a_level);

        if (!childWasSplit) {
            // Child was not split. Merge the bounding box of the new record with the
            // existing bounding box
            a_node->m_branch[index].m_rect = CombineRect(&a_branch.m_rect, &(a_node->m_branch[index].m_rect));
            return false;
        } else {
            // Child was split. The old branches are now re-partitioned to two nodes
            // so we have to re-calculate the bounding boxes of each node
            //            printf("calling NC in InsertRectRec\n");
            a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
            Branch branch;
            branch.m_child = otherNode;
            //            printf("calling NC in InsertRectRec again\n");
            branch.m_rect = NodeCover(otherNode);

            // The old node is already a child of a_node. Now add the newly-created
            // node to a_node as well. a_node might be split because of that.
            return AddBranch(&branch, a_node, a_newNode);
        }
    }
    else if (a_node->m_level == a_level) {
        // We have reached level for insertion. Add rect, split if necessary
        //        cout << "levels were equal calling addbranch\n";
        return AddBranch(&a_branch, a_node, a_newNode);
    } else {
        // Should never occur
        //                ASSERT(0);
        return false;
    }
}


// Inserts a new data rectangle into the index structure.
// Recursively descends tree, propagates splits back up.
// Returns 0 if node was not split.  Old node updated.
// If node was split, returns 1 and sets the pointer pointed to by
// new_node to point to the new node.  Old node updated to become one of two.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRectRec(const Branch &a_branch, Node *a_node, Node **a_newNode, int a_level, Rect &a_coverRect, Rect &a_newCoverRect) {
    //            ASSERT(a_node && a_newNode);
    //            ASSERT(a_level >= 0 && a_level <= a_node->m_level);

    // recurse until we reach the correct level for the new record. data records
    // will always be called with a_level == 0 (leaf)
    if (a_node->m_level > a_level) {
        // Still above level for insertion, go down tree recursively
        Node *otherNode;

        // find the optimal branch for this record
        int index = PickBranch(&a_branch.m_rect, a_node);

        // recursively insert this record into the picked branch
        bool childWasSplit = InsertRectRec(a_branch, a_node->m_branch[index].m_child, &otherNode, a_level, a_coverRect, a_newCoverRect);

        if (!childWasSplit) {
            // Child was not split. Merge the bounding box of the new record with the
            // existing bounding box
            a_node->m_branch[index].m_rect = CombineRect(&a_branch.m_rect, &(a_node->m_branch[index].m_rect));
            return false;
        } else {
            // Child was split. The old branches are now re-partitioned to two nodes
            // so we have to re-calculate the bounding boxes of each node
            //            printf("calling NC in InsertRectRec\n");
            //            a_node->m_branch[index].m_rect = NodeCover(a_node->m_branch[index].m_child);
            a_node->m_branch[index].m_rect = Rect(a_coverRect.m_max, a_coverRect.m_max);
            Branch branch;
            branch.m_child = otherNode;
            //            printf("calling NC in InsertRectRec again\n");
            //            branch.m_rect = NodeCover(otherNode);
            branch.m_rect = Rect(a_newCoverRect.m_min, a_newCoverRect.m_max);

            // The old node is already a child of a_node. Now add the newly-created
            // node to a_node as well. a_node might be split because of that.
            return AddBranch(&branch, a_node, a_newNode, a_coverRect, a_newCoverRect);
        }
    }
    else if (a_node->m_level == a_level) {
        // We have reached level for insertion. Add rect, split if necessary
        //        cout << "levels were equal calling addbranch\n";
        return AddBranch(&a_branch, a_node, a_newNode, a_coverRect, a_newCoverRect);
    } else {
        // Should never occur
        //                ASSERT(0);
        return false;
    }
}



// Insert a data rectangle into an index structure.
// InsertRect provides for splitting the root;
// returns 1 if root was split, 0 if it was not.
// The level argument specifies the number of steps up from the leaf
// level to insert; e.g. a data rectangle goes in at level = 0.
// InsertRect2 does the recursion.
//
RTREE_TEMPLATE
bool RTREE_QUAL::InsertRect(const Branch &a_branch, Node **a_root, int a_level) {
    //            ASSERT(a_root);
    //            ASSERT(a_level >= 0 && a_level <= (*a_root)->m_level);


    Node *newNode;

    //    cout << "in insrertrect\n";

    Rect rect1, rect2;

    if (InsertRectRec(a_branch, *a_root, &newNode, a_level, rect1, rect2))  // Root split
        {
        // Grow tree taller and new root
        Node *newRoot = AllocNode();
        newRoot->m_level = (*a_root)->m_level + 1;
        newRoot->m_parent = NULL;
        // DEBUG

        Branch branch;

        // add old root node as a child of the new root
        //        printf("calling NC in InsertRect\n");

        //        branch.m_rect = NodeCover(*a_root);
        branch.m_rect = Rect(rect1.m_min, rect1.m_max);
        branch.m_child = *a_root;
        AddBranch(&branch, newRoot, NULL);

        // add the split node as a child of the new root
        //        printf("calling NC in InsertRectRec again\n");

        //        branch.m_rect = NodeCover(newNode);
        branch.m_rect = Rect(rect2.m_min, rect2.m_max);
        branch.m_child = newNode;
        AddBranch(&branch, newRoot, NULL);

        // set the new root as the root node
        *a_root = newRoot;

        return true;
        }

    return false;
}



// Find the smallest rectangle that includes all rectangles in branches of a node.
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::NodeCover(Node *a_node) {
    //            ASSERT(a_node);
    Rect rect;
    Rect alaki_rect;
    if( !a_node->IsLeaf() ) {
        //        cout << "NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO\n";
        rect = a_node->m_branch[0].m_rect;
        for (int index = 1; index < a_node->m_count; ++index) {
            rect = CombineRect(&rect, &(a_node->m_branch[index].m_rect));
        }
    } else{
//        rect = m_data_arr[a_node->m_L]->m_rect;
        rect = Rect(m_data_arr_mins[a_node->m_L], m_data_arr_maxes[a_node->m_L]);
        for (int index = a_node->m_L + 1; index < a_node->m_R; ++index) {
//            rect = CombineRect(&rect, &(m_data_arr[index]->m_rect));
            alaki_rect = Rect(m_data_arr_mins[index], m_data_arr_maxes[index]);
            rect = CombineRect(&rect, &(alaki_rect));
        }
    }
    //    cout << "\n" << "here!!! " << rect.m_min[0] << rect.m_min[1] << ", " << rect.m_max[0] << rect.m_max[1] << "\n";
    return rect;
}


// 11 oct
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::LeafCover(int L, int R){
    Rect rect = Rect(m_data_arr_mins[L], m_data_arr_maxes[L]);
    for (int index = L + 1; index < R; ++index) {
        for(int i = 0; i < NUMDIMS; i++){
            if(rect.m_min[i] > m_data_arr_mins[index][i]) rect.m_min[i] = m_data_arr_mins[index][i];
            if(rect.m_max[i] < m_data_arr_maxes[index][i]) rect.m_max[i] = m_data_arr_maxes[index][i];
        }

    }

    // debug
    //    for(int i = 0; i < NUMDIMS; i++){
    //        if(rect.m_min[i] > rect.m_max[i]) {cout << "INJA GHALATE; CHE GOHI KHORDI AKHE. " << i << " " << rect.m_min[i] << " " << rect.m_max[i] << endl;}
    //    }

    return rect;
}



// Add a branch to a node.  Split the node if necessary.
// Returns 0 if node not split.  Old node updated.
// Returns 1 if node split, sets *new_node to address of new node.
// Old node updated, becomes one of two.
RTREE_TEMPLATE
bool RTREE_QUAL::AddBranch(const Branch *a_branch, Node *a_node, Node **a_newNode) {
    //            ASSERT(a_branch);
    //            ASSERT(a_node);
    // regularity check
    if (a_node->isRegular()) {
        int max_children;
        if(a_node->IsLeaf()) {max_children = MAXDATAPOINTS;} else {max_children = MAXNODES;}



        if (a_node->m_count < max_children)  // Split won't be necessary
            {
            if(a_node->IsLeaf()){
//                a_node->m_data_points[a_node->m_count] = *a_branch;
                cout << "INSERTING INTO A LEAF! THIS IS NOT SUPPORTED NOW!!!! \n";
            } else {
                a_node->m_branch[a_node->m_count] = *a_branch;
            }
            if (a_node->m_level > 0) (a_branch->m_child)->m_parent = a_node;

            ++a_node->m_count;
            return false;

            } else {
            // only called on regular nodes!:)
            SplitNode(a_node, a_branch, a_newNode);
            return true;
        }
    } else {
        // is irregular
        // should only be called on irr leaves from the 2dcrack func

        //must be a leaf
        //        printf("adding branch to irregular leaf %d, %d, %d, %d\n", a_branch->m_rect.m_min[0], a_branch->m_rect.m_min[1], a_branch->m_rect.m_max[0], a_branch->m_rect.m_max[1]);
        //                ASSERT(a_node->IsLeaf());
        //        a_node->m_branch.push_back(*a_branch);
        //        ++a_node->m_count;
        cout << "REMEMBER YOU HAVE NOT FIXED THIS!! ADDBRACNH FOR IRREG LEAF\n";
        cout << "NEVER MIND SHOULD NEVER HAVE HAPPENED!\n";
        return false;
    }
}



// Add a branch to a node.  Split the node if necessary.
// Returns 0 if node not split.  Old node updated.
// Returns 1 if node split, sets *new_node to address of new node.
// Old node updated, becomes one of two.
RTREE_TEMPLATE
bool RTREE_QUAL::AddBranch(const Branch *a_branch, Node *a_node, Node **a_newNode, Rect &a_coverRect, Rect &a_newCoverRect) {
    //            ASSERT(a_branch);
    //            ASSERT(a_node);
    // regularity check
    if (a_node->isRegular()) {
        int max_children;
        if(a_node->IsLeaf()) {max_children = MAXDATAPOINTS;} else {max_children = MAXNODES;}




        if (a_node->m_count < max_children)  // Split won't be necessary
            {
            //            printf("simple regular AddBranch\n");
            if(a_node->IsLeaf()){
//                a_node->m_data_points[a_node->m_count] = *a_branch;
                cout << "INSERTING INTO LEAF, NOT SUPPORTED!!!\n";
            } else {
                a_node->m_branch[a_node->m_count] = *a_branch;
            }
            if (a_node->m_level > 0) (a_branch->m_child)->m_parent = a_node;
            //            a_node->m_branch.push_back(*a_branch);
            //            printf("tracking line 1\n");
            ++a_node->m_count;
            //            printf("tracking line 2\n");
            return false;
            //            printf("for fatemeh: %d, %d, %d, %d\n", a_branch->m_rect.m_min[0],a_branch->m_rect.m_min[1],a_branch->m_rect.m_max[0],a_branch->m_rect.m_max[1]);

            } else {
            //                    ASSERT(a_newNode);
            // only called on regular nodes!:)
            //            printf("!!!!!!!!!!!!! had to split !!!!!!!!!!!!!!!!!!!!!!\n");
            SplitNode(a_node, a_branch, a_newNode, a_coverRect, a_newCoverRect);
            return true;
        }
    } else {
        // is irregular
        // should only be called on irr leaves from the 2dcrack func

        //must be a leaf
        //        printf("adding branch to irregular leaf %d, %d, %d, %d\n", a_branch->m_rect.m_min[0], a_branch->m_rect.m_min[1], a_branch->m_rect.m_max[0], a_branch->m_rect.m_max[1]);
        //                ASSERT(a_node->IsLeaf());
        //        a_node->m_branch.push_back(*a_branch);
        //        ++a_node->m_count;
        cout << "REMEMBER YOU HAVE NOT FIXED THIS!! ADDBRACNH FOR IRREG LEAF\n";
        cout << "NEVER MIND SHOULD NEVER HAVE HAPPENED!\n";
        return false;
    }
}


// Disconnect a dependent node.
// Caller must return (or stop using iteration index) after this as count has changed
RTREE_TEMPLATE
void RTREE_QUAL::DisconnectBranch(Node *a_node, int a_index) {
    //            ASSERT(a_node && (a_index >= 0) && (a_index < MAXNODES));
    //            ASSERT(a_node && (a_index >= 0));
    //            ASSERT(a_node);
    //            ASSERT(a_index >=0);
    //            ASSERT(a_node->m_count > 0);

    // Remove element by swapping with the last element to prevent gaps in array
    a_node->m_branch[a_index] = a_node->m_branch[a_node->m_count - 1];
    //    if(a_node->m_branch[a_index].m_child != NULL){
    //        std::vector<Branch>().swap((a_node->m_branch[a_index].m_child)->m_branch);
    //    }
    //    a_node->m_branch[a_index] = a_node->m_branch.back();
    //    a_node->m_branch.pop_back();
    a_node->m_count -= 1;
    //    if(a_node->m_count == 0) {printf("count zero in disB \n");}
    int max_children;
    if(a_node->IsLeaf()) { max_children = MAXDATAPOINTS;} else {max_children = MAXNODES;}
    if (a_node->m_count <= max_children) a_node->m_regular = true;
}




// Pick a branch.  Pick the one that will need the smallest increase
// in area to accomodate the new rectangle.  This will result in the
// least total area for the covering rectangles in the current node.
// In case of a tie, pick the one which was smaller before, to get
// the best resolution when searching.
RTREE_TEMPLATE
int RTREE_QUAL::PickBranch(const Rect *a_rect, Node *a_node) {
    //            ASSERT(a_rect && a_node);

    bool firstTime = true;
    float increase;
    float bestIncr = (float) -1;
    float area;
    float bestArea;
    int best = 0;
    Rect tempRect;

    for (int index = 0; index < a_node->m_count; ++index) {
        Rect *curRect = &a_node->m_branch[index].m_rect;
        area = CalcRectVolume(curRect);
        tempRect = CombineRect(a_rect, curRect);
        increase = CalcRectVolume(&tempRect) - area;
        if ((increase < bestIncr) || firstTime) {
            best = index;
            bestArea = area;
            bestIncr = increase;
            firstTime = false;
        } else if ((increase == bestIncr) && (area < bestArea)) {
            best = index;
            bestArea = area;
            bestIncr = increase;
        }
    }
    return best;
}


// Combine two rectangles into larger one containing both
RTREE_TEMPLATE
typename RTREE_QUAL::Rect RTREE_QUAL::CombineRect(const Rect *a_rectA, const Rect *a_rectB) {
    //            ASSERT(a_rectA && a_rectB);

    Rect newRect;

    for (int index = 0; index < NUMDIMS; ++index) {
        //        printf("IDKY %d, %d \n", a_rectA->m_min[index], a_rectB->m_min[index]);
        newRect.m_min[index] = Min(a_rectA->m_min[index], a_rectB->m_min[index]);
        newRect.m_max[index] = Max(a_rectA->m_max[index], a_rectB->m_max[index]);
    }

    return newRect;
}



// Split a node.
// Divides the nodes branches and the extra one between two nodes.
// Old node is one of the new ones, and one really new one is created.
// Tries more than one method for choosing a partition, uses best result.
RTREE_TEMPLATE
void RTREE_QUAL::SplitNode(Node *a_node, const Branch *a_branch, Node **a_newNode) {
    //            ASSERT(a_node);
    //            ASSERT(a_branch);

    // Could just use local here, but member or external is faster since it is reused
    PartitionVars localVars;
    PartitionVars *parVars = &localVars;

    // Load all the branches into a buffer, initialize old node
    GetBranches(a_node, a_branch, parVars);

    // Find partition
    int min_fill;
    if(a_node->IsLeaf()) {min_fill = MINDATAPOINTS;} else {min_fill = MINNODES;}
    ChoosePartition(parVars, min_fill);

    if(parVars->m_count[0] == 1 || parVars->m_count[1] == 1){
        printf("WHAT THE FUDGE\n");
    }

    // Create a new node to hold (about) half of the branches
    *a_newNode = AllocNode();
    (*a_newNode)->m_level = a_node->m_level;

    // Put branches from buffer into 2 nodes according to the chosen partition
    a_node->m_count = 0;
    //    a_node->m_branch.clear();
    LoadNodes(a_node, *a_newNode, parVars);

    //            ASSERT((a_node->m_count + (*a_newNode)->m_count) == parVars->m_total);
}


RTREE_TEMPLATE
void RTREE_QUAL::SplitNode(Node *a_node, const Branch *a_branch, Node **a_newNode, Rect &a_coverRect, Rect &a_newCoverRect) {
    //            ASSERT(a_node);
    //            ASSERT(a_branch);

    // Could just use local here, but member or external is faster since it is reused
    PartitionVars localVars;
    PartitionVars *parVars = &localVars;

    // Load all the branches into a buffer, initialize old node
    GetBranches(a_node, a_branch, parVars);

    // Find partition
    int min_fill;
    if(a_node->IsLeaf()) {min_fill = MINDATAPOINTS;} else {min_fill = MINNODES;}
    ChoosePartition(parVars, min_fill);

    if(parVars->m_count[0] == 1 || parVars->m_count[1] == 1){
        printf("WHAT THE FUDGE\n");
    }

    // Create a new node to hold (about) half of the branches
    *a_newNode = AllocNode();
    (*a_newNode)->m_level = a_node->m_level;


    // Put branches from buffer into 2 nodes according to the chosen partition
    a_node->m_count = 0;
    //    a_node->m_branch.clear();
    LoadNodes(a_node, *a_newNode, parVars);
    a_coverRect = (parVars->m_cover[0]);
    a_newCoverRect = (parVars->m_cover[1]);

    //            ASSERT((a_node->m_count + (*a_newNode)->m_count) == parVars->m_total);
}


// Calculate the n-dimensional volume of a rectangle
RTREE_TEMPLATE
float RTREE_QUAL::RectVolume(Rect *a_rect) {
    //            ASSERT(a_rect);

    float volume = (float) (a_rect->m_max[0] - a_rect->m_min[0]);
    float len;

    for (int index = 1; index < NUMDIMS; ++index) {
        //        if(volume < 0) {
        //            cout << index << " volume went below zero here\n";
        //            cout << "the rect was: " << a_rect->m_min[0] << " " << a_rect->m_min[1] << " " << a_rect->m_max[0] << " " << a_rect->m_max[1] << endl;
        //        }

        len = a_rect->m_max[index] - a_rect->m_min[index];
        //        if(len < 0) {cout << "len below zero " << a_rect->m_max[index] << " " << a_rect->m_min[index] << " " << len << endl;}
        //                ASSERT(len >= (float) 0);
        //        cout << index << " len here: " << len << endl;
        volume *= (len);
    }
        if(volume < 0) {
            cout << " after for volume went below zero here\n";
            cout << "the rect was: " << a_rect->m_min[0] << " " << a_rect->m_min[1] << " " << a_rect->m_min[2] << " " << a_rect->m_max[0] << " " << a_rect->m_max[1] << " " << a_rect->m_max[2] << endl;
        }

    ASSERT(volume >= (float) 0);

    return volume;
}


// The exact volume of the bounding sphere for the given Rect
RTREE_TEMPLATE
float RTREE_QUAL::RectSphericalVolume(Rect *a_rect) {
    //            ASSERT(a_rect);

    float sumOfSquares = (float) 0;
    float radius;

    for (int index = 0; index < NUMDIMS; ++index) {
        float halfExtent = ((float) a_rect->m_max[index] - (float) a_rect->m_min[index]) * 0.5f;
        sumOfSquares += halfExtent * halfExtent;
    }

    radius = (float) sqrt(sumOfSquares);

    // Pow maybe slow, so test for common dims like 2,3 and just use x*x, x*x*x.
    if (NUMDIMS == 3) {
        return (radius * radius * radius * m_unitSphereVolume);
    } else if (NUMDIMS == 2) {
        return (radius * radius * m_unitSphereVolume);
    } else {
        return (float) (pow(radius, NUMDIMS) * m_unitSphereVolume);
    }
}


// Use one of the methods to calculate retangle volume
RTREE_TEMPLATE
float RTREE_QUAL::CalcRectVolume(Rect *a_rect) {
#ifdef RTREE_USE_SPHERICAL_VOLUME
    return RectSphericalVolume(a_rect); // Slower but helps certain merge cases
#else // RTREE_USE_SPHERICAL_VOLUME
return RectVolume(a_rect); // Faster but can cause poor merges
#endif // RTREE_USE_SPHERICAL_VOLUME
}


// Load branch buffer with branches from full node plus the extra branch.
RTREE_TEMPLATE
void RTREE_QUAL::GetBranches(Node *a_node, const Branch *a_branch, PartitionVars *a_parVars) {
    //            ASSERT(a_node);
    //            ASSERT(a_branch);

    //            ASSERT(a_node->m_count == MAXNODES);
    // TODO technically this should work, seeing as partitioning is just for regular nodes, but make sure
    // Load the branch buffer
    //    for (int index = 0; index < MAXNODES; ++index) {
    //        a_parVars->m_branchBuf[index] = a_node->m_branch[index];
    //    }
    //    a_parVars->m_branchBuf[MAXNODES] = *a_branch;
    int max_children;
    if(a_node->IsLeaf()){max_children = MAXDATAPOINTS;} else {max_children = MAXNODES;}
    for (int index = 0; index < max_children; ++index) {
        //        a_parVars->m_branchBuf.push_back(a_node->m_branch[index]);
        if(a_node->IsLeaf()){
//            a_parVars->m_branchBuf[index] = a_node->m_data_points[index];
// this never happens i think
// but I have to include it any way
//            a_parVars->m_branchBuf[index] = *m_data_arr[a_node->m_L + index];
            a_parVars->m_branchBuf[index] = Branch(NULL, Rect(m_data_arr_mins[a_node->m_L + index], m_data_arr_maxes[a_node->m_L + index]), m_data_arr_ids[a_node->m_L + index]);
        } else {
            a_parVars->m_branchBuf[index] = a_node->m_branch[index];
        }
    }
    //    a_parVars->m_branchBuf.push_back(*a_branch);
    a_parVars->m_branchBuf[max_children] = *a_branch;
    a_parVars->m_branchCount = max_children + 1;

    // Calculate rect containing all in the set
    a_parVars->m_coverSplit = a_parVars->m_branchBuf[0].m_rect;
    for (int index = 1; index < max_children + 1; ++index) {
        a_parVars->m_coverSplit = CombineRect(&a_parVars->m_coverSplit, &a_parVars->m_branchBuf[index].m_rect);
    }
    //    cout << "calling crv in get branches\n";
    a_parVars->m_coverSplitArea = CalcRectVolume(&a_parVars->m_coverSplit);
}


// Method #0 for choosing a partition:
// As the seeds for the two groups, pick the two rects that would waste the
// most area if covered by a single rectangle, i.e. evidently the worst pair
// to have in the same group.
// Of the remaining, one at a time is chosen to be put in one of the two groups.
// The one chosen is the one with the greatest difference in area expansion
// depending on which group - the rect most strongly attracted to one group
// and repelled from the other.
// If one group gets too full (more would force other group to violate min
// fill requirement) then other group gets the rest.
// These last are the ones that can go in either group most easily.
RTREE_TEMPLATE
void RTREE_QUAL::ChoosePartition(PartitionVars *a_parVars, int a_minFill) {
    //            ASSERT(a_parVars);

    float biggestDiff;
    int group, chosen = 0, betterGroup = 0;

    InitParVars(a_parVars, a_parVars->m_branchCount, a_minFill);
    PickSeeds(a_parVars);

    //    cout << "in CHOOSE PARTITION m_total " << a_parVars->m_total << endl;

    while (((a_parVars->m_count[0] + a_parVars->m_count[1]) < a_parVars->m_total)
    && (a_parVars->m_count[0] < (a_parVars->m_total - a_parVars->m_minFill))
    && (a_parVars->m_count[1] < (a_parVars->m_total - a_parVars->m_minFill))) {
        biggestDiff = (float) -1;
        for (int index = 0; index < a_parVars->m_total; ++index) {
            if (PartitionVars::NOT_TAKEN == a_parVars->m_partition[index]) {
                Rect *curRect = &a_parVars->m_branchBuf[index].m_rect;
                Rect rect0 = CombineRect(curRect, &a_parVars->m_cover[0]);
                Rect rect1 = CombineRect(curRect, &a_parVars->m_cover[1]);
                //                cout << "calling crv in choose partition\n";
                float growth0 = CalcRectVolume(&rect0) - a_parVars->m_area[0];
                float growth1 = CalcRectVolume(&rect1) - a_parVars->m_area[1];
                float diff = growth1 - growth0;
                if (diff >= 0) {
                    group = 0;
                } else {
                    group = 1;
                    diff = -diff;
                }

                if (diff > biggestDiff) {
                    biggestDiff = diff;
                    chosen = index;
                    betterGroup = group;
                } else if ((diff == biggestDiff) && (a_parVars->m_count[group] < a_parVars->m_count[betterGroup])) {
                    chosen = index;
                    betterGroup = group;
                }
            }
        }
        //        cout << "CALLING CLASSIFY IN CHOOSEPARTITION FOR " << chosen << endl;
        Classify(chosen, betterGroup, a_parVars);
    }

    // If one group too full, put remaining rects in the other
    if ((a_parVars->m_count[0] + a_parVars->m_count[1]) < a_parVars->m_total) {
        if (a_parVars->m_count[0] >= a_parVars->m_total - a_parVars->m_minFill) {
            group = 1;
        } else {
            group = 0;
        }
        for (int index = 0; index < a_parVars->m_total; ++index) {
            if (PartitionVars::NOT_TAKEN == a_parVars->m_partition[index]) {
                //                cout << "CALLING CLASSIFY IN CHOOSEPARTITION FOR THE REST " << index << endl;

                Classify(index, group, a_parVars);
            }
        }
    }

    //            ASSERT((a_parVars->m_count[0] + a_parVars->m_count[1]) == a_parVars->m_total);
    //            ASSERT((a_parVars->m_count[0] >= a_parVars->m_minFill) &&
    //                   (a_parVars->m_count[1] >= a_parVars->m_minFill));
}


// Copy branches from the buffer into two nodes according to the partition.
RTREE_TEMPLATE
void RTREE_QUAL::LoadNodes(Node *a_nodeA, Node *a_nodeB, PartitionVars *a_parVars) {
    //            ASSERT(a_nodeA);
    //            ASSERT(a_nodeB);
    //            ASSERT(a_parVars);

    for (int index = 0; index < a_parVars->m_total; ++index) {
        //                ASSERT(a_parVars->m_partition[index] == 0 || a_parVars->m_partition[index] == 1);

        int targetNodeIndex = a_parVars->m_partition[index];
        Node *targetNodes[] = {a_nodeA, a_nodeB};

        // It is assured that AddBranch here will not cause a node split.
        bool nodeWasSplit = AddBranch(&a_parVars->m_branchBuf[index], targetNodes[targetNodeIndex], NULL);
        //                ASSERT(!nodeWasSplit);
    }
}


// Initialize a PartitionVars structure.
RTREE_TEMPLATE
void RTREE_QUAL::InitParVars(PartitionVars *a_parVars, int a_maxRects, int a_minFill) {
    //            ASSERT(a_parVars);

    a_parVars->m_count[0] = a_parVars->m_count[1] = 0;
    a_parVars->m_area[0] = a_parVars->m_area[1] = (float) 0;
    a_parVars->m_total = a_maxRects;
    a_parVars->m_minFill = a_minFill;
    for (int index = 0; index < a_maxRects; ++index) {
        a_parVars->m_partition[index] = PartitionVars::NOT_TAKEN;
    }
    //    for (int index = 0; index < a_maxRects; ++index) {
    //        a_parVars->m_partition.push_back(PartitionVars::NOT_TAKEN);
    //    }
}


RTREE_TEMPLATE
void RTREE_QUAL::PickSeeds(PartitionVars *a_parVars) {
    int seed0 = 0, seed1 = 0;
    float worst, waste;
    float area[a_parVars->m_total + 1];

    for (int index = 0; index < a_parVars->m_total; ++index) {
        //        cout << "calling crv in pick seeds\n";
        area[index] = CalcRectVolume(&a_parVars->m_branchBuf[index].m_rect);
    }


    worst = -1 * a_parVars->m_coverSplitArea - 1;
    //    cout << "in pickseeds before for " << a_parVars->m_total << " " << worst << " " << a_parVars->m_coverSplitArea << endl;

    for (int indexA = 0; indexA < a_parVars->m_total - 1; ++indexA) {
        for (int indexB = indexA + 1; indexB < a_parVars->m_total; ++indexB) {
            Rect oneRect = CombineRect(&a_parVars->m_branchBuf[indexA].m_rect, &a_parVars->m_branchBuf[indexB].m_rect);
            //            cout << "calling crv in pick seeds\n";
            waste = CalcRectVolume(&oneRect) - area[indexA] - area[indexB];
            //            cout << "waste: " << waste << endl;
            if (waste > worst) {
                worst = waste;
                seed0 = indexA;
                seed1 = indexB;
                //                cout << "in pickseeds, setting seeds: " << seed0 << " " << seed1 << endl;
            }
        }
    }
    //    cout << "CALLING CLASSIFY IN PICKSEEDS  " << a_parVars->m_total << " " << seed0 << " " << seed1 << endl;
    Classify(seed0, 0, a_parVars);
    Classify(seed1, 1, a_parVars);
}


// Put a branch in one of the groups.
RTREE_TEMPLATE
void RTREE_QUAL::Classify(int a_index, int a_group, PartitionVars *a_parVars) {
    //    cout << "CHECK!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! " << a_index << " " << a_parVars->m_partition[a_index] << endl;
    //            ASSERT(a_parVars);
    //            ASSERT(PartitionVars::NOT_TAKEN == a_parVars->m_partition[a_index]);

    a_parVars->m_partition[a_index] = a_group;

    // Calculate combined rect
    if (a_parVars->m_count[a_group] == 0) {
        a_parVars->m_cover[a_group] = a_parVars->m_branchBuf[a_index].m_rect;
    } else {
        a_parVars->m_cover[a_group] = CombineRect(&a_parVars->m_branchBuf[a_index].m_rect,
                                                  &a_parVars->m_cover[a_group]);
    }

    // Calculate volume of combined rect
    //    cout << "calling crv in classify\n";
    a_parVars->m_area[a_group] = CalcRectVolume(&a_parVars->m_cover[a_group]);

    ++a_parVars->m_count[a_group];
}





// Decide whether two rectangles overlap.
RTREE_TEMPLATE
bool RTREE_QUAL::Overlap(Rect *a_rectA, Rect *a_rectB) const {
    //            ASSERT(a_rectA && a_rectB);

    for (int index = 0; index < NUMDIMS; ++index) {
        if (a_rectA->m_min[index] > a_rectB->m_max[index] ||
        a_rectB->m_min[index] > a_rectA->m_max[index]) {
            return false;
        }
    }
    return true;
}

// Decide whether two rectangles overlap.
RTREE_TEMPLATE
bool RTREE_QUAL::Overlap(Rect *a_rectA, const float a_min[NUMDIMS], const float a_max[NUMDIMS]) const {
    //            ASSERT(a_rectA && a_rectB);

    for (int index = 0; index < NUMDIMS; ++index) {
        if (a_rectA->m_min[index] > a_max[index] ||
        a_min[index] > a_rectA->m_max[index]) {
            return false;
        }
    }
    return true;
}




RTREE_TEMPLATE
bool RTREE_QUAL::Search(Node *a_node, Rect *a_rect, int &a_foundCount, LeavesToAdd *all_lta) {


    if (a_node->IsInternalNode()) {
        // This is an internal node in the tree
        for (int index = 0; index < a_node->m_count; ++index) {
            if (Overlap(a_rect, &a_node->m_branch[index].m_rect)) {
                if (!Search(a_node->m_branch[index].m_child, a_rect, a_foundCount, all_lta)) {
                    // The callback indicated to stop searching
                    return false;
                }
            }
        }
    }
    else {
        if(a_node->isRegular()) {
            for (int index = a_node->m_L; index < a_node->m_R; ++index) {
                if (Overlap(a_rect, m_data_arr_mins[index], m_data_arr_maxes[index])) {
                    ++a_foundCount;
                }
            }

        }
        else{

            Rect node_cover;
            if(a_node->m_parent != NULL) {
                node_cover = ((a_node->m_parent)->m_branch[getNodeBranchIndex(a_node)]).m_rect;
            } else{
                node_cover = root_cover;
            }


            Rect this_piece_rect = Rect(node_cover.m_min, node_cover.m_max);
            int this_piece_L = a_node->m_L;
            int this_piece_R = a_node->m_R;


            int choice, mm, chosen_axis;

            LTA this_lta;
            this_lta.how_many_created =0;
            this_lta.this_leaf = a_node;
            Rect potential_query_piece = Rect(node_cover.m_min, node_cover.m_max); 
            Rect other_piece;


            int crack_index, filan, ashghal;
            int sum_of_choices = 0; int sum_of_mms = 0;

            for(int i =0; i < 2*NUMDIMS; i++){

                for(filan=0; filan < NUMDIMS; filan++) {
                    other_piece.m_min[filan] = potential_query_piece.m_min[filan];
                    other_piece.m_max[filan] = potential_query_piece.m_max[filan];
                }



                if(i == ((2 * NUMDIMS) - 1)){
                    choice = (((2*NUMDIMS) - 1) * NUMDIMS) - sum_of_choices;
                }
                else {
                    choice = ChooseAxisLargestSideMid(this_piece_rect, *a_rect);
                }
                // 11oct 16:26
                if(choice < 0) break;
                
                if(sum_of_mms >= NUMDIMS) mm = 1;
                else if (sum_of_mms <= (-1*NUMDIMS)) mm = 0;
                else {
                    mm = choice % 2;
                    if(mm == 0) sum_of_mms++;
                    else sum_of_mms--;
                }

                chosen_axis = choice / 2;



                if(chosen_axis >= NUMDIMS || chosen_axis < 0) break;

                sum_of_choices += choice;


                other_piece.m_min[chosen_axis] = 21474836;
                other_piece.m_max[chosen_axis] = -21474836;

                if(mm == 0) {
                    potential_query_piece.m_min[chosen_axis] = 21474836;
                    crack_index = CrackOnAxisCompMax( this_piece_L, this_piece_R, a_rect->m_min[chosen_axis],
                                                        chosen_axis, &other_piece, &potential_query_piece);
                }
                else{
                    potential_query_piece.m_max[chosen_axis] = -21474836;
                    crack_index = CrackOnAxisCompMin( this_piece_L, this_piece_R, a_rect->m_max[chosen_axis],
                                                        chosen_axis, &potential_query_piece, &other_piece);
                }



                if((mm == 0 && crack_index != this_piece_L) ||
                (mm == 1 && this_piece_R != crack_index)){

                    if(mm == 0){

                        this_lta.Ls[this_lta.how_many_created] = this_piece_L;
                        this_lta.Rs[this_lta.how_many_created] = crack_index;
                        // set up ptcrack
                        this_piece_L = crack_index;
                    }
                    else{
                        this_lta.Ls[this_lta.how_many_created] = crack_index;
                        this_lta.Rs[this_lta.how_many_created] = this_piece_R;
                        // set up ptcrack
                        this_piece_R = crack_index;
                    }
                    this_piece_rect = Rect(potential_query_piece.m_min, potential_query_piece.m_max);


                    for(ashghal = 0; ashghal < NUMDIMS; ashghal ++){


                        this_lta.crack_covers_mins[this_lta.how_many_created][ashghal] = other_piece.m_min[ashghal];
                        this_lta.crack_covers_maxes[this_lta.how_many_created][ashghal] = other_piece.m_max[ashghal];
                    }

                    this_lta.how_many_created++;
                }
                else{
                    
                    potential_query_piece = Rect(this_piece_rect.m_min, this_piece_rect.m_max);

                    if( mm == 0 ) {this_piece_rect.m_min[chosen_axis] = a_rect->m_min[chosen_axis]; }
                    else {this_piece_rect.m_max[chosen_axis] = a_rect->m_max[chosen_axis];}

                }



                if((this_piece_R - this_piece_L) <= MAXDATAPOINTS) {
                    break;
                }

            }

            // scan last piece
            // DEBUG
            for(ashghal = this_piece_L; ashghal < this_piece_R; ashghal++){
                if (Overlap(a_rect, m_data_arr_mins[ashghal], m_data_arr_maxes[ashghal])) {
                    a_foundCount++;
                }
            }
            // a_foundCount += (this_piece_R - this_piece_L);
            // DEBUG

            if(this_piece_L != this_piece_R) {
                this_lta.Ls[this_lta.how_many_created] = this_piece_L;
                this_lta.Rs[this_lta.how_many_created] = this_piece_R;
                for(ashghal = 0; ashghal < NUMDIMS; ashghal ++){

                    this_lta.crack_covers_mins[this_lta.how_many_created][ashghal] = this_piece_rect.m_min[ashghal];
                    this_lta.crack_covers_maxes[this_lta.how_many_created][ashghal] = this_piece_rect.m_max[ashghal];
                }
                this_lta.how_many_created++;
            }
            all_lta->push_back(this_lta);
        }
    }
    return true; // Continue searching
}


RTREE_TEMPLATE
void RTREE_QUAL::Add_ltas(LeavesToAdd *all_lta){


    for(int i = 0; i < all_lta->size(); i++){
        if((all_lta->at(i).this_leaf)->m_parent != NULL){
            Node* parent_of_start = (all_lta->at(i).this_leaf)->m_parent;
            int branch_index = getNodeBranchIndex((all_lta->at(i).this_leaf));
            DisconnectBranch(parent_of_start, branch_index);
            for(int j = 0 ; j < ((all_lta->at(i)).how_many_created); j++){
                Branch this_branch;
                this_branch.m_child = AllocNode();
                this_branch.m_child->m_L = (all_lta->at(i)).Ls[j];
                this_branch.m_child->m_R = (all_lta->at(i)).Rs[j];

                this_branch.m_child->m_level = 0;
                this_branch.m_child->m_count = this_branch.m_child->m_R - this_branch.m_child->m_L;


                if(this_branch.m_child->m_count > MAXDATAPOINTS) {
                    this_branch.m_child->m_regular = false;
                    this_branch.m_rect = Rect((all_lta->at(i)).crack_covers_mins[j], (all_lta->at(i)).crack_covers_maxes[j]);
                }
                else {
                    this_branch.m_child->m_regular = true;
                    this_branch.m_rect = LeafCover(this_branch.m_child->m_L, this_branch.m_child->m_R);
                }




                Insert_anylevel(this_branch, parent_of_start, 1);
            }
        }
        else{
            m_root->m_count = 0;
            m_root->m_level++;
            m_root->m_regular = true;
            m_root->m_parent = NULL;

            for(int j = 0 ; j < ((all_lta->at(i)).how_many_created); j++){
                Branch this_branch;
                this_branch.m_child = AllocNode();
                this_branch.m_child->m_L = (all_lta->at(i)).Ls[j];
                this_branch.m_child->m_R = (all_lta->at(i)).Rs[j];

                this_branch.m_child->m_level = 0;
                this_branch.m_child->m_count = this_branch.m_child->m_R - this_branch.m_child->m_L;

                if(this_branch.m_child->m_count > MAXDATAPOINTS) {
                    this_branch.m_child->m_regular = false;
                    this_branch.m_rect = Rect((all_lta->at(i)).crack_covers_mins[j], (all_lta->at(i)).crack_covers_maxes[j]);
                }
                else {
                    this_branch.m_child->m_regular = true;
                    this_branch.m_rect = LeafCover(this_branch.m_child->m_L, this_branch.m_child->m_R);
                }


                InsertRect(this_branch, &m_root, 1);
            }
        }
    }
}


RTREE_TEMPLATE
bool RTREE_QUAL::CountNodes(Node *a_node, int &a_foundCount){
    a_foundCount += 1;
    if (a_node->IsInternalNode()) {
        for (int index = 0; index < a_node->m_count; ++index) {
            CountNodes(a_node->m_branch[index].m_child, a_foundCount);
        }
    }
    return true; // Continue searching
}


RTREE_TEMPLATE
int RTREE_QUAL::ChooseAxisLargestSideMid(Rect node_cover, Rect query_cover) {

    float this_ms;
    float current_max_m = std::abs(node_cover.m_max[0] - node_cover.m_min[0]);
    int current_max_index = 0;
    for(int i =1; i < NUMDIMS; i++){
        this_ms = std::abs(node_cover.m_max[i] - node_cover.m_min[i]);
        if(this_ms > current_max_m){
            current_max_m = this_ms;
            current_max_index = i;
        }
    }
    //    cout << "in choose, last " << current_max_m << endl;

    // now we have to figure out if q_min[current_max_index] ids closer to mid of node_cover
    float mid_of_node_cover_axis = (float) ((node_cover.m_max[current_max_index] + node_cover.m_min[current_max_index]) / 2);

    if(std::abs(query_cover.m_min[current_max_index] - mid_of_node_cover_axis) <
    std::abs(query_cover.m_max[current_max_index] - mid_of_node_cover_axis)){
//                cout << "chose min and axis " << current_max_index << endl;
        return (2 * current_max_index);
    } else{
//                cout << "chose max and axis " << current_max_index << endl;
        return (2 * current_max_index + 1);
    }
}


RTREE_TEMPLATE
int RTREE_QUAL::CrackOnAxisCompMin( int L, int R, const float& crack_value, int axis, Rect *left_rect, Rect *right_rect) {
    return MyPartition(L, R, crack_value, axis, true, left_rect, right_rect);
}


RTREE_TEMPLATE
int RTREE_QUAL::CrackOnAxisCompMax( int L, int R, const float& crack_value, int axis, Rect *left_rect, Rect *right_rect) {
    return MyPartition(L, R, crack_value, axis, false, left_rect, right_rect);
}


RTREE_TEMPLATE
void RTREE_QUAL::swap_index(int i, int j) {
    //6oct
    //id
    std::swap(m_data_arr_ids[i], m_data_arr_ids[j]);
    //mins
    std::swap(m_data_arr_mins[i], m_data_arr_mins[j]);
    //maxes
    std::swap(m_data_arr_maxes[i], m_data_arr_maxes[j]);

}


RTREE_TEMPLATE
int RTREE_QUAL::MyPartition(int low, int high, float pivot_value, int axis, bool min_or_max, Rect *left_rect, Rect *right_rect){
    int x1 = low; int x2 = high - 1;
    Rect x1_rect, x2_rect;



    if(min_or_max){
        while(x1 <= x2 && x2 > 0){
            if (m_data_arr_mins[x1][axis] < pivot_value) {
                // update left cover

//                if(m_data_arr_mins[x1][axis] < left_rect->m_min[axis]){left_rect->m_min[axis] = m_data_arr_mins[x1][axis];}
                if(m_data_arr_maxes[x1][axis] > left_rect->m_max[axis]){left_rect->m_max[axis] = m_data_arr_maxes[x1][axis];}

                x1++;
            }
            else{
                while(x2 > 0 && x2 >= x1 && m_data_arr_mins[x2][axis] >= pivot_value){
                    // update right cover

                    if(m_data_arr_mins[x2][axis] < right_rect->m_min[axis]){right_rect->m_min[axis] = m_data_arr_mins[x2][axis];}
                    if(m_data_arr_maxes[x2][axis] > right_rect->m_max[axis]){right_rect->m_max[axis] = m_data_arr_maxes[x2][axis];}

                    x2--;
                }

                if(x1 < x2){
                    swap_index(x1, x2);
                    // update left cover

//                    if(m_data_arr_mins[x1][axis] < left_rect->m_min[axis]){left_rect->m_min[axis] = m_data_arr_mins[x1][axis];}
                    if(m_data_arr_maxes[x1][axis] > left_rect->m_max[axis]){left_rect->m_max[axis] = m_data_arr_maxes[x1][axis];}

                    // update right cover

                    if(m_data_arr_mins[x2][axis] < right_rect->m_min[axis]){right_rect->m_min[axis] = m_data_arr_mins[x2][axis];}
                    if(m_data_arr_maxes[x2][axis] > right_rect->m_max[axis]){right_rect->m_max[axis] = m_data_arr_maxes[x2][axis];}

                    x2--; x1++;
                }
            }
        }
    }
    else{
        while(x1 <= x2 && x2 > 0){
            if (m_data_arr_maxes[x1][axis] < pivot_value) {
                // update left cover

                if(m_data_arr_mins[x1][axis] < left_rect->m_min[axis]){
                    left_rect->m_min[axis] = m_data_arr_mins[x1][axis];

                }
                if(m_data_arr_maxes[x1][axis] > left_rect->m_max[axis]){
                    left_rect->m_max[axis] = m_data_arr_maxes[x1][axis];

                }

                x1++;

            }
            else{
                while(x2 > 0 && x2 >= x1 && m_data_arr_maxes[x2][axis] >= pivot_value){
                    // update right cover
                    if(m_data_arr_mins[x2][axis] < right_rect->m_min[axis]){
                        right_rect->m_min[axis] = m_data_arr_mins[x2][axis];

                    }




                    x2--;
                }
                if(x1 < x2){
                    swap_index(x1, x2);
                    // update left cover

                    if(m_data_arr_mins[x1][axis] < left_rect->m_min[axis]){
                        left_rect->m_min[axis] = m_data_arr_mins[x1][axis];

                    }
                    if(m_data_arr_maxes[x1][axis] > left_rect->m_max[axis]){
                        left_rect->m_max[axis] = m_data_arr_maxes[x1][axis];

                    }

                    // update right cover
                    if(m_data_arr_mins[x2][axis] < right_rect->m_min[axis]){
                        right_rect->m_min[axis] = m_data_arr_mins[x2][axis];

                    }



                    x2--; x1++;


                }
            }
        }
    }

    return x1;
}


RTREE_TEMPLATE
void RTREE_QUAL::CopyBranch(Branch &current, const Branch &other){
    current.m_rect = other.m_rect;
    current.m_child = other.m_child;
    current.m_data = other.m_data;
}


RTREE_TEMPLATE
int RTREE_QUAL::getNodeBranchIndex(Node *a_node) {
    //    cout << "in get index"<< (a_node->m_parent)->m_branch.size() << "\n";
    //    ASSERT(a_node);
    //    cout << a_node->m_id << " has " << a_node->m_count << " children\n";
    //    cout << a_node->m_id << " parent children count in getnodebranchindex " << (a_node->m_parent)->m_branch.size() << ", " << (a_node->m_parent)->m_count <<"\n";
    //    cout << "checking levels: " << a_node->m_level << " " << (a_node->m_parent)->m_level << "\n";
    // JUST FOR CHECKING
    //    for(int i = 0; i < (a_node->m_parent)->m_branch.size(); i++) {
    //        cout << "in getnodebranchindex for checkkkkk " << ((a_node->m_parent)->m_branch[i].m_child)->m_id << "\n";
    //    }
    for(int i = 0; i < (a_node->m_parent)->m_count; i++) {
        //        cout << "in getnodebranchindex for " << ((a_node->m_parent)->m_branch[i].m_child)->m_id << "\n";
        if (a_node->m_id == ((a_node->m_parent)->m_branch[i].m_child)->m_id) {
            return i;
        }
    }
    return -1;
}

RTREE_TEMPLATE
void RTREE_QUAL::Insert_anylevel(const Branch &a_branch, Node *start, int a_level){
    //    ASSERT(start);
    //    ASSERT(start->m_level >= a_level);
    //    cout << "in insert_any \n";
    if(start->m_level > a_level){
        printf("calling pb in insert_anylevel\n");
        int index = PickBranch(&a_branch.m_rect, start);

        Insert_anylevel(a_branch, start->m_branch[index].m_child, a_level);
    }
    else{
        // start->m_level == a_level
        Node * current_node = start;
        Branch current_branch;
        CopyBranch(current_branch, a_branch);
        bool is_split = false; // so the loop starts
        bool just_go_till_top = false; // to indicate that no more adds are required, but we should recurse up to update the rects
        while(true){
            //            cout << "in while\n";
            // last current_node was not root
            if(!just_go_till_top) {
                //                cout << "in while not just till top\n";
                Node *newNode = AllocNode();
                Rect rect1; Rect rect2;
                is_split = AddBranch(&current_branch, current_node, &newNode, rect1, rect2);

                if (is_split) {
                    //                    cout << "in while was split\n";
                    // was split
                    if (current_node->m_parent != NULL) {
                        //                        cout << "in while is split, not root\n";
                        // update nodecover rectangles
                        //                        cout << "CHECK POINT 1\n";
                        int index = getNodeBranchIndex(current_node);
                        //                        (current_node->m_parent)->m_branch[index].m_rect = NodeCover(current_node);
                        (current_node->m_parent)->m_branch[index].m_rect = rect1;
                        // set parameters to move up the tree
                        current_branch.m_child = newNode;
                        //                        current_branch.m_rect = NodeCover(newNode);
                        current_branch.m_rect = rect2;
                        current_node = current_node->m_parent;
                    } else {
                        //                        cout << "in while is split and root\n";
                        // current_node is root and it was split
                        // now we have to make a new root

                        Node *newRoot = AllocNode();
                        newRoot->m_level = current_node->m_level + 1;
                        newRoot->m_parent = NULL;

                        current_branch.m_child = newNode;
                        //                        current_branch.m_rect = NodeCover(newNode);
                        current_branch.m_rect = rect2;

                        AddBranch(&current_branch, newRoot, NULL);

                        current_branch.m_child = current_node;
                        //                        current_branch.m_rect = NodeCover(current_node);
                        current_branch.m_rect = rect1;

                        AddBranch(&current_branch, newRoot, NULL);

                        m_root = newRoot;
                        break;
                    }
                } else {
                    //                    cout << "in while not split\n";
                    // was not split
                    if (current_node->m_parent != NULL) {
                        //                        cout << "in while not split and not root\n";
                        // we are not at the root
                        //                        cout << "CHECK POINT 2 " << current_node->m_id << " with parent:: " << (current_node->m_parent)->m_id << "\n" ;
                        int index = getNodeBranchIndex(current_node);
                        (current_node->m_parent)->m_branch[index].m_rect = CombineRect(&(current_branch.m_rect),
                                                                                       &((current_node->m_parent)->m_branch[index].m_rect));
                        current_node = current_node->m_parent;
                        just_go_till_top = true;
                    } else {

                        //                        cout << "THIS SHOULD NOT HAPPEN, or maybe it could\n";
                        break;
                    }
                }
            } else{
                //                cout << "in while just till top\n";
                if (current_node->m_parent != NULL) {
                    //                    cout << "in while just till top not root\n";
                    //                    cout << "CHECK POINT 3\n";
                    int index = getNodeBranchIndex(current_node);
                    //                    cout << "after get index\n";
                    (current_node->m_parent)->m_branch[index].m_rect = CombineRect(&(current_branch.m_rect),
                                                                                   &((current_node->m_parent)->m_branch[index].m_rect));
                    //                    (current_node->m_parent)->m_branch[index].m_rect = NodeCover(current_node);

                    current_node = current_node->m_parent;
                } else{
                    //                    cout << "in while just till top root\n";
                    break;
                }
            }

        }

    }
}


#undef RTREE_TEMPLATE
#undef RTREE_QUAL

#endif //RTREE

